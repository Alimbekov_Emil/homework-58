import React from "react";
import Backdrop from "../Backdrop/Backdrop";
import "./Modal.css";

const Modal = (props) => {
  return (
    <>
      <Backdrop show={props.show} onClick={props.closed} />
      <div
        className="Modal"
        style={{
          transform: props.show ? "translateX(0)" : "translateX(-100vh)",
          opacity: props.show ? "1" : "0",
        }}
      >
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">{props.title}</h5>
            <button className="btn-close" onClick={props.closed} />
          </div>
          <div className="modal-body">{props.children}</div>
          <div className="modal-footer">
            <button className="btn btn-success" onClick={props.continued}>
              Сontinue
            </button>
            <button className="btn btn-primary" onClick={props.closed}>
              Close
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Modal;
