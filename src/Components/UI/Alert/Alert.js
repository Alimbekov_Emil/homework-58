import React from "react";
import "./Alert.css";
const Alert = (props) => {
  if (props.dismiss === undefined) {
    return (
      <>
        <div
          className={["Alert", props.type].join(" ")}
          style={{
            transform: props.show ? "translateX(0)" : "translateX(-100vh)",
            opacity: props.show ? "1" : "0",
          }}
        >
          {props.children}
        </div>
      </>
    );
  }
  
  return (
    <>
      <div
        className={["Alert", props.type].join(" ")}
        style={{
          transform: props.show ? "translateY(0)" : "translateY(-100vh)",
          opacity: props.show ? "1" : "0",
        }}
      >
        {props.children}
        <button onClick={props.dismiss}>X</button>
      </div>
    </>
  );
};

export default Alert;
