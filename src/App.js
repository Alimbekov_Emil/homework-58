import React, { useState } from "react";
import "./App.css";
import Alert from "./Components/UI/Alert/Alert";
import Modal from "./Components/UI/Modal/Modal";

const App = () => {
  const [showModal, setShowModal] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  const showMyModal = () => {
    setShowModal(true);
  };

  const closedMyModal = () => {
    setShowModal(false);
  };
  const continuedModal = () => {
    alert("Your Continued");
    setShowModal(false);
  };

  const showMyAlert = () => {
    setShowAlert(true);
  };

  const closedMyAlert = () => {
    setShowAlert(false);
  };

  return (
    <div className="App">
      <Modal
        show={showModal}
        title="Some kinda modal title"
        closed={closedMyModal}
        continued={continuedModal}
      >
        <p>This is modal content</p>
      </Modal>

      <Alert show={showAlert} type="primary">
        This is a primary type alert
      </Alert>
      <Alert show={showAlert} type="warning" dismiss={closedMyAlert}>
        This is a warning type alert
      </Alert>
      <Alert show={showAlert} type="success">
        This is a success type alert
      </Alert>
      <Alert show={showAlert} type="danger" dismiss={closedMyAlert}>
        This is a danger type alert
      </Alert>

      <button className="btn btn-success" onClick={showMyModal}>
        Modal
      </button>
      <button className="btn btn-danger" onClick={showMyAlert}>
        Alert
      </button>
    </div>
  );
};

export default App;
